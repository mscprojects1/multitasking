'use strict'

// Globaalsed muutujad andmete jaoks
var ID = new Date().getTime();
console.log(ID);

//Random choice which test will be opened
Array.prototype.sample = function(){
    return this[Math.floor(Math.random()*this.length)];
  }

// 0 on paralleelne
// 1 on järjestikune  
var testgroup = [0,1].sample();
//console.log("algne",testgroup);

// 0 - esimene on T1
// 1 - esimene on T2
var esimene = [0,1].sample();
//console.log("esimene", esimene);

var sugu = "";
var synniaasta = "";
var haridus = "";
var amet = "";

var Q1 = "";
var Q2 = "";
var Q3 = "";
var Q41 = "";
var Q42 = "";
var Q43 = "";
var Q5 = "";
var Q6 = "";
var T10 = "";
var T11 = "";
var T12 = "";
var T13 = "";
var T14 = "";
var T15 = "";
var T16 = "";
var T17 = "";
var T18 = "";
var T19 = "";
var K10 = "";
var K11 = "";
var K12 = "";
var K13 = "";
var K14 = "";

var T20 = "";
var T21 = "";
var T22 = "";
var T23 = "";
var T24 = "";
var T25 = "";
var T26 = "";
var T27 = "";
var T28 = "";
var T29 = "";
var K20 = "";
var K21 = "";
var K22 = "";
var K23 = "";
var K24 = "";

var E1 = "";
var E11 = "";
var E2 = "";
var E3 = "";
var E4 = "";

var cltimeout = "";

//Button listener lehele index html
const switcher = document.querySelector('#alusta');
switcher.addEventListener('click', function() {
        // eemaldatakse 1. lehekülg koos sisuga
    document.getElementById("algus").remove();
    pageSurvey();
});

function pageSurvey() {
    var br = document.createElement("br");

    //lisatakse survey lehe sisu
    var dv = document.createElement('div');
    dv.id = "survey";
    document.getElementById("main").appendChild(dv);

    var h = document.createElement('h1');
    h.id = "kys";
    h.textContent = "Küsimustik";
    document.getElementById("survey").appendChild(h);

    var frm = document.createElement('div');
    frm.id = "frm";
    document.getElementById("survey").appendChild(frm);

    var man = document.createElement('p');
    man.id = "man";
    man.textContent = "Palun vasta kõikidele küsimustele. Enne ei saa edasi liikuda testide juurde.";
    document.getElementById("frm").appendChild(man);

    var surv =  document.createElement('form');
    surv.id = "surv";
    surv.className = "surv";
    document.getElementById("frm").appendChild(surv);

    var dv1 = document.createElement('div');
    dv1.className = "form-check";
    dv1.id = "dv1";
    document.getElementById("surv").appendChild(dv1);

    var lb1 = document.createElement('label');
    lb1.className = "lbkys";
    lb1.textContent = "1. Sinu sugu: ";
    document.getElementById("dv1").appendChild(lb1);

    var dvmees = document.createElement('div');
    dvmees.id = "dvmees";
    document.getElementById("dv1").appendChild(dvmees);

    var in1 = document.createElement('input');
    in1.type = "radio";
    in1.id = "mees";
    in1.name = "sugu";
    in1.value = "M";
    in1.required = true;
    document.getElementById("dvmees").appendChild(in1);

    var lb2 = document.createElement('label');
    lb2.for = "mees";
    lb2.id = "lb2";
    lb2.textContent = "mees (M)";
    lb2.className = "lbradio";
    document.getElementById("dvmees").appendChild(lb2);

    var dvnaine = document.createElement('div');
    dvnaine.id = "dvnaine";
    document.getElementById("dv1").appendChild(dvnaine);
    
    var in2 = document.createElement('input');
    in2.type = "radio";
    in2.id = "naine";
    in2.name = "sugu";
    in2.value = "N";
    document.getElementById("dvnaine").appendChild(in2);

    var lb3 = document.createElement('label');
    lb3.for = "naine";
    lb3.textContent = "naine (N)";
    lb3.className = "lbradio";
    document.getElementById("dvnaine").appendChild(lb3);

    var dv2 = document.createElement('div');
    dv2.className = "form-check";
    dv2.id = "dv2";
    document.getElementById("surv").appendChild(dv2);

    var lb4 = document.createElement('label');
    lb4.className = "lbkys";
    lb4.textContent = "2. Sinu sünniaasta: ";
    document.getElementById("dv2").appendChild(lb4);

    var sel1 = document.createElement('select');
    sel1.name = "yearpicker";
    sel1.id = "yearpicker";
    sel1.required = true;
    document.getElementById("dv2").appendChild(sel1);

    // Aastate rippmenüü lehele survey.html
    let startYear = 1949;
    let endYear = 2010;
    $('#yearpicker').append($('<option />').val(null).html(""));
    
    for (var i = endYear; i > startYear; i--) {
        $('#yearpicker').append($('<option />').val(i).html(i));
    }

    var dv3 = document.createElement('div');
    dv3.className = "form-check";
    dv3.id = "haridus";
    document.getElementById("surv").appendChild(dv3);

    var lb5 = document.createElement('label');
    lb5.className = "lbkys";
    lb5.textContent = "3. Sinu haridustase: ";
    document.getElementById("haridus").appendChild(lb5);

    var sel2 = document.createElement('select');
    sel2.name = "edu";
    sel2.id = "edu";
    sel2.required = true;
    document.getElementById("haridus").appendChild(sel2);

    //Hariduse valik lehele survey.html
    var edu = [ "põhiharidus", 
                "kutseõpe põhihariduse baasil", 
                "keskharidus", 
                "kutseõpe keskhariduse baasil", 
                "rakenduslik kõrgharidus",
                "kõrgharidus (sh bakalaureus)", 
                "magister", 
                "doktor"];

    $('#edu').append($('<option />').val(null).html("")); 

    for (i = 0; i < edu.length; i++) {
        $('#edu').append($('<option />').val(edu[i]).html(edu[i]));
    }
    //Checkboxi lahendus, hetkel kasutusel radio buttoni lahendus
   /* for (i = 0; i < edu.length; i ++) {
        var br = document.createElement('br')
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.id = "h" + i.toString();
        checkbox.name = "h" + i.toString();
        checkbox.value = edu[i]
        document.getElementById("haridus").appendChild(checkbox);
        var lbl = document.createElement("label");
        lbl.for = "h" + i.toString();
        lbl.textContent = edu[i];
        document.getElementById("haridus").appendChild(lbl);
        document.getElementById("haridus").appendChild(br);
    }*/
    //}

    var dv4 = document.createElement('div');
    dv4.className = "form-check";
    dv4.id = "kutse";
    document.getElementById("surv").appendChild(dv4);

    var lb6 = document.createElement('label');
    lb6.className = "lbkys";
    lb6.textContent = "4. Sinu amet: ";
    document.getElementById("kutse").appendChild(lb6);

    var sel3 = document.createElement('select');
    sel3.name = "amet";
    sel3.id = "amet";
    sel3.required = true;
    document.getElementById("kutse").appendChild(sel3);
    document.getElementById("kutse").appendChild(br);

    // Ametite valik lehele survey.html
    var kutse = [
        "sõjaväelane",
        "juht",
        "tippspetsialist",
        "tehnik / keskastme spetsialist",
        "ametnik",
        "teenindus / müügitöötaja",
        "põllumajanduse / metsanduse / jahinduse / kalanduse oskustöötaja",
        "oskustöötaja / käsitööline",
        "seadme- ja masinaoperaator / koostaja",
        "lihttööline",
        "ei tööta",
        "üliõpilane",
        "muu"
        ];
    $('#amet').append($('<option />').val(null).html(""));

    for (i = 0; i < kutse.length; i++) {
        $('#amet').append($('<option />').val(kutse[i]).html(kutse[i]));
    }

    const selectElement = document.querySelector('#amet');

    selectElement.addEventListener('change', (event) => {
    var selam = $("#amet :selected").val();
    var sel = false;

    if(selam == "muu" && sel == false) {
        var dv = document.createElement('div')
        dv.id = "uuskutse"
        document.getElementById("kutse").appendChild(dv);
        document.getElementById("uuskutse").appendChild(br);
        var lbl = document.createElement("label");
        lbl.id = "muukutselbl"
        lbl.textContent = "Kuna valisid muu, siis palun sisesta oma amet: "
        document.getElementById("uuskutse").appendChild(lbl);
        document.getElementById("uuskutse").appendChild(br);
        var muu = document.createElement("input");
        muu.type = "text";
        muu.id = "muukutse";
        muu.name = "lisaamet"
        document.getElementById("uuskutse").appendChild(muu);
        sel = true;
    } else {
        document.getElementById("uuskutse").remove();
        sel = false;
    }
    });
//}
    var dv5 = document.createElement('div');
    dv5.className = "form-check";
    dv5.id = "varem";
    document.getElementById("surv").appendChild(dv5);

    var lb7 = document.createElement('label');
    lb7.className = "lbkys";
    lb7.textContent = "5. Kas Sul on olnud vaja rööprähelda, st samaaegselt erinevaid ülesandeid täita? Nt midagi kuulata ja samal ajal lugeda või nutiseadmes midagi teha?";
    document.getElementById("varem").appendChild(lb7);

    //varasem multitaskingu kogemus lehele survey.html
    var valaeg = ["päevas mitu korda",
                "päevas korra",
                "nädalas mõned korrad",
                "nädalas korra",
                "üldse ei pea"
            ];
    
    for (i = 0; i < valaeg.length; i ++) {
        var br = document.createElement('br')
        var aeg = document.createElement("input");
        if (i == 0) aeg.required = true;
        aeg.type = "radio";
        aeg.id = "v" + i.toString();
        aeg.name = "v"
        aeg.value = valaeg[i]
        document.getElementById("varem").appendChild(aeg);
        var lbl = document.createElement("label");
        lbl.for = "v" + i.toString();
        lbl.className = "lbradio";
        lbl.textContent = valaeg[i];
        document.getElementById("varem").appendChild(lbl);
        document.getElementById("varem").appendChild(br);
    }

    var dv6 = document.createElement('div');
    dv6.className = "form-check";
    dv6.id = "hea";
    document.getElementById("surv").appendChild(dv6);

    var lb8 = document.createElement('label');
    lb8.className = "lbkys";
    lb8.textContent = "6. Kui heaks rööprähklejaks Sa ennast pead: ";
    document.getElementById("hea").appendChild(lb8);

    //varasem multitaskingu oskus lehele survey.html
    var valhea = ["saan väga hästi hakkama",
                "saan hakkama",
                "ei tule eriti hästi välja",
                "üldse ei õnnestu"
            ];

    for (i = 0; i < valhea.length; i ++) {
        var br = document.createElement('br')
        var h = document.createElement("input");
        if (i == 0) h.required = true;
        h.type = "radio";
        h.id = "hea" + i.toString();
        h.name = "hea"
        h.value = valhea[i]
        document.getElementById("hea").appendChild(h);
        var lbl = document.createElement("label");
        lbl.className = "lbradio";
        lbl.for = "hea" + i.toString();
        lbl.textContent = valhea[i];
        document.getElementById("hea").appendChild(lbl);
        document.getElementById("hea").appendChild(br);
    }

    var dv7 = document.createElement('div');
    dv7.className = "form-check";
    dv7.id = "raal";
    document.getElementById("surv").appendChild(dv7);

    var lb9 = document.createElement('label');
    lb9.className = "lbkys";
    lb9.textContent = "7. Kui heaks arvutikasutajaks Sa ennast pead?";
    document.getElementById("raal").appendChild(lb9);

    //arvutikasutamise oskus lehele survey.html
    var valraal = [
        "igapäevaselt",
        "nädalas mõne korra",
        "korra nädalas",
        "korra kuus"
    ];

    for (i = 0; i < valraal.length; i ++) {
        var br = document.createElement('br')
        var h = document.createElement("input");
        if (i == 0) h.required = true;
        h.type = "radio";
        h.id = "raal" + i.toString();
        h.name = "raal";
        h.value = valhea[i];
        document.getElementById("raal").appendChild(h);
        var lbl = document.createElement("label");
        lbl.for = "raal" + i.toString();
        lbl.className = "lbradio";
        lbl.textContent = valhea[i];
        document.getElementById("raal").appendChild(lbl);
        document.getElementById("raal").appendChild(br);
    }

    var dv8 = document.createElement('div');
    dv8.className = "form-check";
    dv8.id = "miks";
    document.getElementById("surv").appendChild(dv8);

    var lb10 = document.createElement('label');
    lb10.className = "lbkys";
    lb10.textContent = "8. Milleks Sa arvutit kasutad (vali kõik sobivad): ";
    document.getElementById("miks").appendChild(lb10);

    //arvutikasutamine lehele survey.html
    var valmiks = ["õppimiseks / töötamiseks enamlevinud kontoritarkvaraga", 
                "mängimiseks", 
                "programmeerimiseks"
            ];
        
    for (i = 0; i < valmiks.length; i ++) {
        var br = document.createElement('br')
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.id = "miks" + i.toString();
        checkbox.name = "miks" + i.toString();
        checkbox.value = valmiks[i]
        document.getElementById("miks").appendChild(checkbox);
        var lbl = document.createElement("label");
        lbl.for = "miks" + i.toString();
        lbl.className = "lbradio";
        lbl.textContent = valmiks[i];
        document.getElementById("miks").appendChild(lbl);
        document.getElementById("miks").appendChild(br);
    }

    var dv9 = document.createElement('div');
    dv9.className = "form-check";
    dv9.id = "dv9";
    document.getElementById("surv").appendChild(dv9);

    var lb11 = document.createElement('label');
    lb11.className = "lbkys";
    lb11.textContent = "9. Kumba Sa kasutad rohkem: ";
    document.getElementById("dv9").appendChild(lb11);

    var dvnuti = document.createElement('div');
    dvnuti.id = "dvnuti";
    document.getElementById("dv9").appendChild(dvnuti);

    var in2 = document.createElement('input');
    in2.type = "radio";
    in2.id = "nuti";
    in2.name = "rohkem";
    in2.value = "nutiseade";
    in2.required = true;
    document.getElementById("dvnuti").appendChild(in2);

    var lb12 = document.createElement('label');
    lb12.for = "nuti";
    lb12.className = "lbradio";
    lb12.textContent = "mobiilset nutiseadet";
    document.getElementById("dvnuti").appendChild(lb12);

    var dvarvut = document.createElement('div');
    dvarvut.id = "dvarvut";
    document.getElementById("dv9").appendChild(dvarvut);

    var in3 = document.createElement('input');
    in3.type = "radio";
    in3.id = "arvut";
    in3.name = "rohkem";
    in3.value = "arvuti";
    document.getElementById("dvarvut").appendChild(in3);

    var lb13 = document.createElement('label');
    lb13.for = "arvut";
    lb13.className = "lbradio";
    lb13.textContent = "arvutit";
    document.getElementById("dvarvut").appendChild(lb13);

    var dv10 = document.createElement('div');
    dv10.className = "form-check";
    dv10.id = "dv10";
    document.getElementById("surv").appendChild(dv10);

    var lb14 = document.createElement('label');
    lb14.className = "lbkys";
    lb14.textContent = "10. Kas Sa loed igapäevaselt tekste, mis on pikemad kui 1 lehekülg?";
    document.getElementById("dv10").appendChild(lb14);

    var in4 = document.createElement('input');
    in4.type = "radio";
    in4.id = "jah";
    in4.name = "tekst";
    in4.value = "jah";
    in4.required = true;
    document.getElementById("dv10").appendChild(in4);

    var lb15 = document.createElement('label');
    lb15.for = "jah";
    lb15.className = "lbradio";
    lb15.textContent = "jah";
    document.getElementById("dv10").appendChild(lb15);
    document.getElementById("dv10").appendChild(br);

    var in5 = document.createElement('input');
    in5.type = "radio";
    in5.id = "ei";
    in5.name = "tekst";
    in5.value = "ei";
    document.getElementById("dv10").appendChild(in5);

    var lb16 = document.createElement('label');
    lb16.for = "ei";
    lb16.textContent = "ei";
    lb16.className = "lbradio";
    document.getElementById("dv10").appendChild(lb16);

    var b1 = document.createElement("input");
    b1.type = "submit";
    b1.className = "btn btn-primary btn-sm";
    b1.id = "esitasurv";
    b1.value = "Saada vastused";
    document.getElementById("surv").appendChild(b1);

    // Form listener lehele survey.html - esitamine
    document.querySelector('form.surv').addEventListener('submit', (e) => {
                   
        const formData = new FormData(e.target);
        sugu = formData.get("sugu");
        //console.log(sugu, formData.get("sugu"));
        synniaasta = formData.get("yearpicker");
        //console.log(synniaasta, formData.get("yearpicker"));
        haridus = formData.get("edu");
        //console.log(haridus, formData.get("edu"));
        amet = formData.get("amet");
        //console.log(formData.get("amet"));
        if (amet == "muu") {
            //console.log(formData.get("lisaamet"));
            amet = formData.get("lisaamet"); 
        }
        //console.log(amet);
        Q1 = formData.get("v");
        //console.log(Q1, formData.get("v"));
        Q2 = formData.get("hea");
        //console.log(Q2, formData.get("hea"));
        Q3 = formData.get("raal");
        //.log(Q3, formData.get("raal"));
        Q41 = formData.get("miks0");
        //console.log(Q41, formData.get("miks0"));
        Q42 = formData.get("miks1");
        //console.log(Q42, formData.get("miks1"));
        Q43 = formData.get("miks2");
        //console.log(Q43, formData.get("miks2"));
        Q5 = formData.get("rohkem");
        //console.log(Q5, formData.get("rohkem"));
        Q6 = formData.get("tekst");
        //console.log(Q6, formData.get("tekst"));

        var survVal = {
            "entry.2028255309": ID,
            "entry.264848878": sugu,
            "entry.2015444177": synniaasta,
            "entry.101174682": haridus,
            "entry.1900193318": amet,
            "entry.2125601395": Q1,
            "entry.2001583220": Q2,
            "entry.683009490": Q3,
            "entry.491514258": Q41,
            "entry.433873215": Q42,
            "entry.441301279": Q43,
            "entry.1307664623": Q5,
            "entry.470157970": Q6,
            };

        $.ajax({
                url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSc2FbBW22yvP-Dbb2zv2O09bhwpFyC6sF9ivVPGzPUGWdvs1Q/formResponse",
                data: survVal,
                type: "POST",
                dataType: "xml",
                statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
                }
            });

        e.preventDefault();
        document.getElementById("man").remove();
        document.getElementById("surv").remove();

        var br = document.createElement('br')
        var t1 = document.createElement("div");
        t1.id = "kystxt";
        document.getElementById("frm").appendChild(t1);

        var t2 = document.createElement("p");
        t2.textContent = "Täname vastuste eest! On aeg suunduda testi tegema. Test on küll pingeline, kuid ära heida meelt. See on täiesti ootuspärane, kui mõned küsimused jäävad vastamata."
        document.getElementById("kystxt").appendChild(t2);
        //document.getElementById("kystxt").appendChild(br);

        var t3 = document.createElement("p");
        t3.textContent = "Järgmise lehe alguses on kirjas, millist liiki ülesandeid tuleb lahedada ja kas neid on vaja lahendada paralleelselt või järjestikku."
        document.getElementById("kystxt").appendChild(t3);

        var t5 = document.createElement("p");
        t5.id = "t5";
        t5.textContent = "Ära kasuta testi tegemise ajal 'Back' või 'Tagasi' nuppu ega noolt, sest see tühistab kogu testi ja viib algusesse.";
        document.getElementById("kystxt").appendChild(t5);

        var t4 = document.createElement("button");
        t4.type = "button";
        t4.className = "btn btn-primary btn-sm";
        t4.name = "esimene";
        t4.id = "esimene";
        t4.textContent = "Liigun esimese testi juurde";
        document.getElementById("frm").appendChild(t4);

        const switcher1 = document.querySelector('#esimene');
        switcher1.addEventListener('click', function() {
            //Kustutab senise ja liigub järgmise osa juurde
            document.getElementById("survey").remove();
            //pageT1();
            if (esimene === 0) {
                pageT1();
            } else {
                pageT2();
            }
            
        });
    });
}

//Esimese testi küsimused
var kysimused = [
    '1. Milline sõna on kõige sarnasemad fraasile "kahe jalaga maa peal"?',
    "2. Milline ei sõna ei kuulu teistega samasse liiki:",
    "3. Vali sõna, mis on kõige vastupidisem sõnale: VÕLTS",
    "4. Milline järgnevatest ei ole puu?",
    "5. Milline sõna lõpetab vanasõna ‘IGAÜKS ON OMA ÕNNE …’",
    "6. Milline sõna sobib vanasõnasse mõlema küsimärgi asemele 'AMET EI ? MEEST, KUI MEES AMETIT EI ?'",
    "7. Vali sõna, mis on tähenduse poolest kõige sarnasem või lähedasem sõnale: LOJAALNE",
    "8. Külluse vastandiks on:",
    "9. JÄÄR ja LAMMAS on suhestatud samamoodi nagu TÄKK ja",
    "10. Kuidas on sõna ‘piisk’ mitmuse vorm?",
];
 
//Esimese testi vastused
var vastused = [
    ["kuulekas", "ruraalne", "maalähedane", "täpne", "realistlik"],
    ["murel", "ploom", "kirss", "õun", "jõhvikas"],
    ["räpane", "autentne", "plagiaat", "särav", "jäik"],
    ["mänd", "kuusk", "kadakas", "tamm", "kask"],
    ["tegija", "looja", "sepp", "otsija", "leidja" ],
    ["toida", "laida", "riku", "kiida", "solva"],
    ["elujõuline", "teadlik", "kaitsev", "truu", "valiv"],
    ["luksus", "andmus", "puudus", "pühendumus", "läbikukkumine"],
    ["hobune", "hirv", "siga", "veis", "koer"],
    ["piisa", "piiska", "piisad", "vihm", "piiskad"]
];

//Esimese kuulmisülesande küsimused
var lugkys = [
    "1. Millest see lugu räägib?",
    "2. Miks oli Vanaisa loonud maailma loomad?",
    "3. Miks kutsus Vanaisa loomad kokku?",
    "4. Miks on hundi nina musta värvi?",
    "5. Miks on vähil silmad kuklas?"
];

//Esimese kuulmisülesande vastused
var lugvastused = [
    ["See lugu räägib sellest, et kõik maailma jõed on loomade kaevatud.",
    "See lugu räägib sellest, kuidas võis sündida üks suur Eesti jõgi.",
    "See lugu räägib sellest, kuidas on kõige lihtsam jõgesid kaevata."],
    ["Sellepärast, et igaüks elust rõõmu tunneks.",
    "Sellepärast, et loomad saaksid jõge kaevata.",
    "Sellepärast, et loomadele kuningas määrata."],
    ["Ta tahtis loomadega jõe kaevamise üle nõu pidada.",
    "Loomad ei saanud omavahel läbi.",
    "Vanaisa tahtis loomi kiusata."],
    ["Sellepärast, et hunt töötas sügaval.",
    "Sellepärast, et hunt mängis töö ajal poriga.",
    "Sellepärast, et hunt töötas oma koonuga palju."],
    ["Sellepärast, et Vanaisa märkas teda liiga hilja.",
    "Sellepärast, et vähk kündis esimese vao.",
    "Sellepärast, et ta käitus Vanaisaga nipsakalt."]
    ];

//Audio küsimustele vastamise aeg, kasutatakse T1 ja T2
//Peaks olema 1 min praegu. Vb on liiga palju
var timeforkuulamine = 60000;

//T1 audio ja selle metaandmete lugemine, et tuvastada pikkust, mida kasutatakse järjestikkuse versiooni jaoks
var audiofile1 = 'emajogi.mp3'
//var audiofile1 = 'doris.mp3'
var audio = new Audio(audiofile1);
var audioduration = 0;
audio.onloadedmetadata = function() {
    audioduration = audio.duration*1000;
    //console.log(audioduration);
};

function pageT1 (){
    testgroup = Math.abs(testgroup-1);
    //console.log("T1", testgroup);
    var first = document.createElement("div");
    first.id = "first";
    document.getElementById("main").appendChild(first);

    var terv1 = document.createElement("h1");
    terv1.id = "terv1";
    if (esimene === 0) {
        terv1.textContent = "Esimene test";
    } else {
        terv1.textContent = "Teine test";
    }
    
    document.getElementById("first").appendChild(terv1);

    var terv13 = document.createElement("h3");
    terv13.id = "terv13";
    terv13.textContent = "Kuulamisülesanne ja verbaalsed testid";
    document.getElementById("first").appendChild(terv13);

    var tr = document.createElement("p");
    tr.id = "testryhm";
    tr.textContent = "";
    document.getElementById("first").appendChild(tr);
    
    //teavitatakse, millisesse rühma kuulub
    var tryhm = document.getElementById("testryhm");
    if (testgroup == 0) {
        tryhm.textContent = "Oled testrühmas, kelle ülesanded on paralleelsed, st kuulamisülesandega tuleb samaaegselt lahendada teste ja seejärel vastata küsimustele kuuldud jutu kohta."
    } else {
        tryhm.textContent = "Oled testrühmas, kelle ülesanded on järjestikused, st kõigepealt on testid, siis algab kuulamisülesanne, seejärel küsimused kuuldud jutu kohta."
    }

    var yl = document.createElement("div");
    yl.id = "yl";
    yl.className = "yl";
    document.getElementById("first").appendChild(yl);

    var julge = document.createElement("p");
    julge.id = "julge";
    julge.textContent = "Kui Sa oled valmis alustama, siis kliki nupule! Ära karda, kõik läheb hästi!";
    document.getElementById("yl").appendChild(julge);

    var e1 = document.createElement("button");
    e1.type = "button";
    e1.className = "btn btn-primary btn-sm";
    e1.id = "e1";
    e1.textContent = "Alustan";
    document.getElementById("yl").appendChild(e1);

    //Button listener T1 alustamise nupule
    const switcher2 = document.querySelector('#e1');
    switcher2.addEventListener('click', function() {
        document.getElementById("julge").remove();
        document.getElementById("e1").remove();
        if (testgroup === 0) {
            parallel();
        } else {
            consec();
        } 
    });
}
//}

//Abifunktsioon küsimuste vastuste kuvamiseks (tekstküsimus + valikvastused (radiobutton))
function nextQ (c, kysimused, vastused, clname) {
    var dvf = document.createElement("div");
    dvf.id = "fdiv";
    document.getElementById("f1").appendChild(dvf);

    var lbl = document.createElement("label");
    lbl.className = "lbkys"
    lbl.id = c;
    lbl.textContent = kysimused[c];
    document.getElementById("fdiv").appendChild(lbl);
    var vastus = vastused[c];
        
    for (var i = 0; i < vastus.length; i ++) {
       /* var br = document.createElement('br');
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.id = "vastus" + i.toString();
        checkbox.name = "vastus" + i.toString();
        checkbox.value = vastus[i]
        document.getElementById("fdiv").appendChild(checkbox);
        var lblv = document.createElement("label");
        lblv.for = "vast" + i.toString();
        lblv.textContent = vastus[i];
        document.getElementById("fdiv").appendChild(lblv);*/

        var br = document.createElement('br')
        var vas = document.createElement("input");
        //if (i == 0) aeg.required = true;
        vas.type = "radio";
        vas.id = "t" + String(c) + String(i);
        //console.log(vas.id);
        vas.name = "tt" + String(c);
        //console.log(vas.name);
        vas.value = vastus[i];
        document.getElementById("fdiv").appendChild(vas);
        var lblv = document.createElement("label");
        lblv.for = "t" + String(c) + String(i);
        lblv.className = "lbradio";
        lblv.textContent = vastus[i];
        document.getElementById("fdiv").appendChild(lblv);
        document.getElementById("fdiv").appendChild(br);

        if (clname == "lugv") {
            document.getElementById("fdiv").appendChild(br);
        }
    }
}

// Abifunktsioon küsimuste ja audio paralleelseks esitamiseks T1
function parallel () {
    var br = document.createElement('br');
    var lugu = document.createElement("p");
    lugu.id = "lugu"
    lugu.textContent = 'Kuulamisülesanne: ';
    document.getElementById("yl").appendChild(lugu);
    var lugu1 = document.createElement("p");
    lugu1.id = "lugu1"
    lugu1.innerHTML = 'Kuulete müüti "Emajõe sünd". Loeb Tõnis Hendrik Hlebnikov.<br>';
    document.getElementById("yl").appendChild(lugu1);
    var allk1 = document.createElement("p");
    allk1.id = "allk1";
    allk1.innerHTML = 'Allikas: https://eis.ekk.edu.ee/eis/lahendamine/3012/edit';
    document.getElementById("yl").appendChild(allk1);
    var lugu2 = document.createElement("p");
    lugu2.id = "lugu2"
    lugu2.textContent = "Ülesandeid on võimalik lahendada loo ajal. Pärast lugu esitatakse küsimused kuuldud loo kohta."
    document.getElementById("yl").appendChild(lugu2);

    var f = document.createElement("form");
    f.id = "f1";
    f.className = "t1";
    f.name = "t1";
    document.getElementById("yl").appendChild(f);

    var saadetud = false;
    var start = 0;

    audio = new Audio(audiofile1);
    audio.addEventListener("canplaythrough", event => {
        audio.play();
        audio.addEventListener("play", function(e){
            var lugu3 = document.createElement("p");
            lugu3.textContent = "KÜSIMUSED:"
            lugu3.id = "lugu3"
            document.getElementById("f1").appendChild(lugu3);

            var c = 0;
            while (c < 10) {
                nextQ(c, kysimused, vastused,"");
                c++;
            }
            
            start = new Date().getTime(); 

            document.getElementById("fdiv").appendChild(br);
            var bt = document.createElement("input");
            bt.type = "submit";
            bt.className = "btn btn-primary btn-sm";
            bt.id ="conf";
            bt.value = "Lõpeta";
            bt.name = "fin";
            document.getElementById("fdiv").appendChild(bt);

            var allt1 = document.createElement("p");
            allt1.id = "allt1";
            allt1.innerHTML = 'Allikas: Carteri, P. “IQ ja psühhomeetrilised testid” (London, 2007). Eesti keelde tõlkinud Kreet Volmer.';
            document.getElementById("fdiv").appendChild(allt1);

            document.querySelector('form.t1').addEventListener('submit', (e) => {
                if (!saadetud) {
                    var end = new Date().getTime();
                    getT1Testdata(e, end-start);
                    document.getElementById("f1").remove();
                    lugu2.textContent = "Vastused on esitatud! Kuula lugu lõpuni ja vasta seejärel loo kohta esitatud küsimustele!"
                    document.getElementById("yl").appendChild(lugu2);
                    saadetud = true;
                }
                e.preventDefault();
            });
        });
    });

    audio.addEventListener("ended", function(e){
        if (!saadetud) {
            document.getElementById('conf').click();
        }
        saadetud = false;
        if (document.getElementById("f1")) {
            document.getElementById("f1").remove();
        }
        document.getElementById("lugu").remove();
        document.getElementById("lugu1").remove();
        document.getElementById("lugu2").remove();
        
        f = document.createElement("form");
        f.id = "f1";
        f.className = "k1";
        f.name = "k1";
        document.getElementById("yl").appendChild(f);

        var c = 0;
        while (c < 5) {
            nextQ(c, lugkys, lugvastused, "lugv")
            c++;
        }
        var startK = new Date().getTime();;

        var bt = document.createElement("input");
        bt.type = "submit";
        bt.className = "btn btn-primary btn-sm";
        bt.id ="conf";
        bt.value = "Lõpeta";
        document.getElementById("fdiv").appendChild(bt);

        document.querySelector('form.k1').addEventListener('submit', (e) => {
            if (!saadetud) {
                var endK = new Date().getTime();
                getK1data(e, endK-startK);
                saadetud = true;
                clearTimeout(cltimeout);
                remQ(saadetud, startK);
            }
            e.preventDefault();
        });
        if (!saadetud) cltimeout = setTimeout(remQ, timeforkuulamine, saadetud, startK);
    });
}

// Abifunktsioon T1 järjestikku kuvamiseks
function consec () {
    var br = document.createElement('br');
    var lugu = document.createElement("p");
    lugu.id = "lugu"
    lugu.textContent = 'Testid: ';
    document.getElementById("yl").appendChild(lugu);
    var lugu2 = document.createElement("p");
    lugu2.id = "lugu2"
    lugu2.textContent = "Lahenda nii palju teste kui oskad ja nii kiiresti kui suudad. Testi lahendamine on ajaliselt piiratud kuulmisülesande kestusega. Aja lõppedes lõpeb test automaatselt."
    document.getElementById("yl").appendChild(lugu2);
    
    var f = document.createElement("form");
    f.id = "f1";
    f.className = "t1";
    f.name = "t1";
    document.getElementById("yl").appendChild(f);

    var c = 0;
    while (c < 10) {
        nextQ(c, kysimused, vastused,"");
        c++;
    }

    var start = new Date().getTime();
    var saadetud = false;

    document.getElementById("fdiv").appendChild(br);
    
    var bt = document.createElement("input");
    bt.type = "submit"
    bt.className = "btn btn-primary btn-sm"
    bt.id ="conf"
    bt.value = "Lõpeta"
    document.getElementById("fdiv").appendChild(bt);

    var allt1 = document.createElement("p");
    allt1.id = "allt1";
    allt1.innerHTML = 'Allikas: Carteri, P. “IQ ja psühhomeetrilised testid” (London, 2007). Eesti keelde tõlkinud Kreet Volmer.';
    document.getElementById("fdiv").appendChild(allt1);


    document.querySelector('form.t1').addEventListener('submit', (e) => {
        if (!saadetud){
            var end = new Date().getTime();
            getT1Testdata(e, end-start);
            saadetud = true;
            clearTimeout(cltimeout);
            //remT(saadetud);
            vaheNupp(saadetud)
        }
        e.preventDefault();
    });

    //if (!saadetud) cltimeout = setTimeout(remT, audioduration, saadetud);
    if (!saadetud) cltimeout = setTimeout(vaheNupp, audioduration, saadetud);
}

function vaheNupp(saadetud) {
    if (!saadetud) {
        document.getElementById('conf').click();
    }
    saadetud = false;
    document.getElementById("f1").remove();
    document.getElementById("lugu").remove();
    document.getElementById("lugu2").remove();

    var julge = document.createElement("p");
    julge.id = "julge";
    julge.textContent = "Oled testi sooritanud, aeg on asuda kuulamisülesande juurde.";
    document.getElementById("yl").appendChild(julge);

    var e1 = document.createElement("button");
    e1.type = "button";
    e1.className = "btn btn-primary btn-sm";
    e1.id = "e1";
    e1.textContent = "Alustan kuulamisülesannet";
    document.getElementById("yl").appendChild(e1);

    //Button listener T1 alustamise nupule
    const switcher2 = document.querySelector('#e1');
    switcher2.addEventListener('click', function() {
        document.getElementById("julge").remove();
        document.getElementById("e1").remove();
        remT(saadetud);
    });
}

//Abifunktsioon järjestikkuse testi lõpetamiseks T1 ja kuulamisülesande käivitamiseks
function remT(saadetud) {
    /*if (!saadetud) {
        document.getElementById('conf').click();
    }
    saadetud = false;
    document.getElementById("f1").remove();
    document.getElementById("lugu").remove();
    document.getElementById("lugu2").remove();*/

    audio = new Audio(audiofile1);
    audio.addEventListener("canplaythrough", event => {
        audio.play();
        audio.addEventListener("play", function(e){
            var lugu = document.createElement("p");
            lugu.id = "lugu";
            lugu.textContent = 'Kuulamisülesanne: ';
            document.getElementById("yl").appendChild(lugu);
            var lugu1 = document.createElement("p");
            lugu1.id = "lugu1";
            lugu1.innerHTML = 'Kuulete müüti "Emajõe sünd". Loeb Tõnis Hendrik Hlebnikov. <br>';
            document.getElementById("yl").appendChild(lugu1);
            var allk1 = document.createElement("p");
            allk1.id = "allk1";
            allk1.innerHTML = 'Allikas: https://eis.ekk.edu.ee/eis/lahendamine/3012/edit';
            document.getElementById("yl").appendChild(allk1);
            var lugu2 = document.createElement("p");
            lugu2.id = "lugu2";
            lugu2.textContent = "Kuula lugu ära ja vasta seejärel küsimustele. Küsimustele vastamine on ajaliselt piiratud."
            document.getElementById("yl").appendChild(lugu2);
        });
    });
    audio.addEventListener("ended", function(e){
        document.getElementById("lugu").remove();
        document.getElementById("lugu1").remove();
        document.getElementById("lugu2").remove();    

        var f = document.createElement("form");
        f.id = "f1";
        f.className = "k1";
        f.name = "k1";
        document.getElementById("yl").appendChild(f);
    
        var c = 0;
        while (c < 5) {
            nextQ(c, lugkys, lugvastused, "lugv");
            c++;
        }
        
        var startK = new Date().getTime();

        var bt = document.createElement("input");
        bt.type = "submit";
        bt.className = "btn btn-primary btn-sm";
        bt.id ="conf";
        bt.value = "Lõpeta";
        document.getElementById("fdiv").appendChild(bt);
    
        document.querySelector('form.k1').addEventListener('submit', (e) => {
            if (!saadetud) {
                var endK = new Date().getTime();
                getK1data(e, endK-startK);
                saadetud = true;
                clearTimeout(cltimeout);
                remQ(saadetud, startK)
            }
            e.preventDefault();
        });
    
        if (!saadetud) cltimeout = setTimeout(remQ, timeforkuulamine, saadetud, startK);
    });
}

//Abifunktsioon lugemisküsimuste lõpetamiseks
function remQ(saadetud, startK) {
    if (!saadetud) {
        document.getElementById('conf').click();
        document.querySelector('form.k1').addEventListener('submit', (e) => {
            var endK = new Date().getTime();
             getK1data(e, endK-startK);
             saadetud = true;
             e.preventDefault();
        });
    }
    document.getElementById("first").remove();
    //pageT2();
    if (esimene === 0) {
        pageT2();
    } else {
        pageEnd();
    }
    
}

//Teise testi küsimused
var kysimused2 = ["1. Mitu tikku on pildil?", 
                "2. Täringu vastastahkudel asuvate silmade summa on 7. Milline on täringutest moodustatud torni kahel mittenähtaval küljel asuvate silmade summa?",
                "3. Milline element sobib tühja lahtrisse?",
                "4. Milline vaade avaneb noolega näidatud suunast?",
                "5. Kui tasapinnal lamavasse rõngaks painutatud torusse paisata suure hooga kuul, siis milline on selle kuuli torust väljalendamise trajektoor?",
                "6. Kui köiejuppi tõmmata kahest otsast, millisest moodustub sõlm?",
                "7. Milline arv on kõige väiksem?",
                "8. Milline kuuest märgist sobib küsimärgi asemele?",
                "9. Mitu klotsi võiks olla hinnanguliselt selles klotsihunnikus?",
                "10. Milline element sobib ritta järgmisena?",
                ];


//Teise testi vastused
var vastused2 = [
    ["A","B","C","D","E", "F"],
    ["A","B","C","D"],
    ["A","B","C","D"],
    ["A","B","C","D"],
    ["A","B","C","D"],
    ["A","B","C","D","E"],
    ["A","B","C","D","E", "F"],
    ["A","B","C","D","E", "F"],
    ["A","B","C","D","E"],
    ["A","B","C","D"]
];
                
//Teise testi kuulamisülesande küsimused
var lugkys2 = ["1. Millest see artikkel räägib?",
            "2. Miks lapsed leiutavad?",
            "3. Miks osaletakse leiutamisvõistlusel?",
            "4. Miks on joogipudel edukas?",
            "5. Miks toimuvad leiutamistunnid kuni 9. klassini?"
            ];

//Teise testi kuulamisülesande vastused
var lugvastused2 = [
            ["Sellest, kuidas teadlaseks saada.", "Sellest, kuidas võistlust korraldada.", "Sellest, kuidas lapsed leiutavad."],
            ["Lapsed leiutavad seepärast, et neil on tunnis igav.", "Lapsed leiutavad seepärast, et lahendada igapäevaelu probleeme.","Lapsed leiutavad seepärast, et saada hinne."],
            ["Leiutamisvõistlusel osalemine on populaarne.", "Leiutamisvõistlusel osalemine on nohikutele.","Leiutamisvõistlusel osalemine ei huvita õpilasi."],
            ["Joogipudel on edukas, sest selle leiutas insener.","Joogipudel on edukas, sest seda ostetakse palju.","Joogipudel on edukas, sest seda on mugav täita."],
            ["Lapsed ei jõua järgmistes klassides enam leiutamisega tegeleda.","Laste jaoks pole vanemates klassides leiutamine enam oluline.","Laste loovus muutub."],
            ]

//Teise testi kuulamisülesande audio ja selle pikkuse tuvastamine
var audiofile2 = "leiuta.mp3";
//var audiofile2 = "doris2.mp3"
var audio2 = new Audio(audiofile2);
var audioduration2 = 0;
audio2.onloadedmetadata = function() {
    audioduration2 = audio2.duration*1000;
    //console.log(audioduration2);
};

function pageT2 () {
    testgroup = Math.abs(testgroup-1);
    //console.log("T2", testgroup);
    var second = document.createElement("div");
    second.id = "second";
    document.getElementById("main").appendChild(second);

    var terv1 = document.createElement("h1");
    terv1.id = "terv1";
    terv1.textContent = "Teine test";
    if (esimene === 0) {
        terv1.textContent = "Teine test";
    } else {
        terv1.textContent = "Esimene test";
    }
    document.getElementById("second").appendChild(terv1);

    var terv13 = document.createElement("h3");
    terv13.id = "terv13";
    terv13.textContent = "Kuulamisülesanne ning matemaatilised ja ruumilise taju testid";
    document.getElementById("second").appendChild(terv13);

    var tr = document.createElement("p");
    tr.id = "testryhm";
    tr.textContent = "";
    document.getElementById("second").appendChild(tr);

    var yl = document.createElement("div");
    yl.id = "yl";
    yl.className = "yl";
    document.getElementById("second").appendChild(yl);

    var julge = document.createElement("p");
    julge.id = "julge";
    julge.textContent = "Kui Sa oled valmis alustama, siis kliki nupule! Ära karda, kõik läheb hästi!";
    document.getElementById("yl").appendChild(julge);

    var e1 = document.createElement("button");
    e1.type = "button";
    e1.className = "btn btn-primary btn-sm";
    e1.id = "e1";
    e1.textContent = "Alustan";
    document.getElementById("yl").appendChild(e1);

    var tryhm = document.getElementById("testryhm");
    if (testgroup == 0) {
        tryhm.textContent = "Oled testrühmas, kelle ülesanded on paralleelsed, st kuulamisülesandega tuleb samaaegselt lahendada teste ja seejärel vastata küsimustele kuuldud jutu kohta."
    } else {
        tryhm.textContent = "Oled testrühmas, kelle ülesanded on järjestikused, st kõigepealt on testid, siis algab kuulamisülesanne, seejärel küsimused kuuldud jutu kohta."
    }

    const switcher3 = document.querySelector('#e1');
    switcher3.addEventListener('click', function() {
        var br = document.createElement('br');
        document.getElementById("julge").remove();
        document.getElementById("e1").remove();
        if (testgroup === 0) {
            parallel2();
        } else {
            consec2();
        } 
    });
}

//T2 abifunktsioon pildiga ülesannete kuvamiseks
function nextQImg (c, kysimused, vastused, clname) {
    var dvf = document.createElement("div");
    dvf.id = "fdiv"
    document.getElementById("f1").appendChild(dvf);

    var lbl = document.createElement("label");
    lbl.className = "lbkys";
    lbl.textContent = kysimused[c];
    document.getElementById("fdiv").appendChild(lbl);
    var vastus = vastused[c];

    var im = document.createElement("img");
    var n = String(c+1);
    im.src = n.concat(".JPG");
    //console.log(n.concat(".JPG"));
    document.getElementById("fdiv").appendChild(im);

    //Versioon, kus vastused on sisestatavad (hetkel kasutusel valikvastused)
    /*var input = document.createElement("input");
    input.type = "text";
    input.id = "tt" + String(c);
    input.name = "tt" + String(c);
    document.getElementById("fdiv").appendChild(input);*/

    for (var i = 0; i < vastus.length; i ++) {
         var br = document.createElement('br')
         var vas = document.createElement("input");
         vas.type = "radio";
         vas.className = "t2v";
         vas.id = "t" + String(c) + String(i);
         //console.log(vas.id);
         vas.name = "tt" + String(c);
         //console.log(vas.name);
         vas.value = vastus[i];
         document.getElementById("fdiv").appendChild(vas);
         var lblv = document.createElement("label");
         lblv.for = "t" + String(c) + String(i);
         lblv.className = "lbradio";
         lblv.textContent = vastus[i];
         document.getElementById("fdiv").appendChild(lblv);
 
         if (clname == "lugv") {
             document.getElementById("fdiv").appendChild(br);
         }
     }
}

//Abifunktsioon T2 ülesannete paralleelseks läbimiseks
function parallel2 () {
    var br = document.createElement('br');
    var lugu = document.createElement("p");
    lugu.id = "lugu"
    lugu.textContent = 'Kuulamisülesanne: ';
    document.getElementById("yl").appendChild(lugu);
    var lugu1 = document.createElement("p");
    lugu1.id = "lugu1"
    lugu1.innerHTML = 'Kuulete artiklit "Rootsi lapsed õpivad koolitunnis leiutama". Loeb Tõnis Hendrik Hlebnikov. <br>';
    document.getElementById("yl").appendChild(lugu1);
    var allk2 = document.createElement("p");
    allk2.id = "allk2";
    allk2.innerHTML = 'Allikas: https://eis.ekk.edu.ee/eis/lahendamine/3302/edit';
    document.getElementById("yl").appendChild(allk2);
    var lugu2 = document.createElement("p");
    lugu2.id = "lugu2"
    lugu2.textContent = "Ülesandeid on võimalik lahendada loo ajal. Pärast lugu esitatakse küsimused kuuldud loo kohta. Küsimustele vastamine on ajaliselt piiratud."
    document.getElementById("yl").appendChild(lugu2);

    var f = document.createElement("form");
    f.id = "f1";
    f.className = "t2";
    f.name = "t2";
    document.getElementById("yl").appendChild(f);

    var saadetud = false;

    audio2 = new Audio(audiofile2);
    audio2.addEventListener("canplaythrough", event => {
        audio2.play();
        audio2.addEventListener("play", function(e){
            var lugu3 = document.createElement("p");
            lugu3.textContent = "KÜSIMUSED:"
            lugu3.id = "lugu3"
            document.getElementById("f1").appendChild(lugu3);

           var  c = 0;
            while (c < 10) {
                nextQImg(c, kysimused2, vastused2, "");
                c++;
            }
            var start = new Date().getTime();
            document.getElementById("fdiv").appendChild(br);
            var bt = document.createElement("input");
            bt.type = "submit"
            bt.className = "btn btn-primary btn-sm"
            bt.id ="conf"
            bt.value = "Lõpeta"
            document.getElementById("fdiv").appendChild(bt);

            var allt2 = document.createElement("p");
            allt2.id = "allt2";
            allt2.innerHTML = 'Allikad: <br>';
            allt2.innerHTML += 'https://www.researchgate.net/publication/294693864_Intelligentsuse_psuhholoogia/link/56c2f53e08aee5caccfbbea6/download<br>';
            allt2.innerHTML += 'https://www.mensa.lu/en/mensa/online-iq-test/online-iq-test.html <br>';
            allt2.innerHTML += 'https://www.teachstarter.com/wp-content/uploads/2016/08/New-brain-blog-banner.png<br>';
            allt2.innerHTML += 'Carteri, P. “IQ ja psühhomeetrilised testid” (London, 2007). Eesti keelde tõlkinud Kreet Volmer.';
            document.getElementById("fdiv").appendChild(allt2);

            document.querySelector('form.t2').addEventListener('submit', (e) => {
                if (!saadetud) {
                    var end = new Date().getTime();
                    getT2Testdata(e, end-start);
                    lugu2.textContent =  "Vastused on esitatud! Kuula lugu lõpuni ja vasta seejärel loo kohta esitatud küsimustele!"; 
                    document.getElementById("f1").remove();
                    saadetud = true;
                }
                e.preventDefault();
            });
    
        });
    });

    audio2.addEventListener("ended", function(e){
        if (!saadetud) {
            document.getElementById('conf').click();
        }
        saadetud = false;
        if (document.getElementById("f1")) {
            document.getElementById("f1").remove();
        }
        document.getElementById("lugu").remove();
        document.getElementById("lugu1").remove();
        document.getElementById("lugu2").remove();

        var f = document.createElement("form");
        f.id = "f1";
        f.className = "k2";
        f.name = "k2";
        document.getElementById("yl").appendChild(f);

        var c = 0;
        while (c < 5) {
            nextQ(c, lugkys2, lugvastused2, "lugv")
            c++;
        }
        var startK = new Date().getTime();

        var bt = document.createElement("input");
        bt.type = "submit";
        bt.className = "btn btn-primary btn-sm";
        bt.id ="conf";
        bt.value = "Lõpeta";
        document.getElementById("fdiv").appendChild(bt);

        document.querySelector('form.k2').addEventListener('submit', (e) => {
            if (!saadetud) {
                var endK = new Date().getTime();
                getK2data(e, endK-startK);
                saadetud = true;
                clearTimeout(cltimeout);
                remQ2(saadetud, startK);
            }
            e.preventDefault();
        });
        if (!saadetud) cltimeout = setTimeout(remQ2, timeforkuulamine, saadetud, startK);
    });
}

// Abifunktsioon T2 järjestikku kuvamiseks
function consec2 () {
    var br = document.createElement('br');
    var lugu = document.createElement("p");
    lugu.id = "lugu"
    lugu.textContent = 'Testid: ';
    document.getElementById("yl").appendChild(lugu);
    var lugu2 = document.createElement("p");
    lugu2.id = "lugu2"
    lugu2.textContent = "Lahenda nii palju teste kui oskad ja nii kiiresti kui suudad. Testi lahendamine on ajaliselt piiratud kuulmisülesande kestusega. Aja lõppedes lõpeb test automaatselt."
    document.getElementById("yl").appendChild(lugu2);
    
    var f = document.createElement("form");
    f.id = "f1";
    f.className = "t2";
    f.name = "t2";
    document.getElementById("yl").appendChild(f);

    var c = 0;
    while (c < 10) {
        nextQImg(c, kysimused2, vastused2, "");
        c++;
    }
    var start = new Date().getTime();
    var saadetud = false;

    document.getElementById("fdiv").appendChild(br);
    var bt = document.createElement("input");
    bt.type = "submit"
    bt.className = "btn btn-primary btn-sm"
    bt.id ="conf"
    bt.value = "Lõpeta"
    document.getElementById("fdiv").appendChild(bt);

    var allt2 = document.createElement("p");
    allt2.id = "allt2";
    allt2.innerHTML = 'Allikad: <br>';
    allt2.innerHTML += 'https://www.researchgate.net/publication/294693864_Intelligentsuse_psuhholoogia/link/56c2f53e08aee5caccfbbea6/download<br>';
    allt2.innerHTML += 'https://www.mensa.lu/en/mensa/online-iq-test/online-iq-test.html <br>';
    allt2.innerHTML += 'https://www.teachstarter.com/wp-content/uploads/2016/08/New-brain-blog-banner.png<br>';
    allt2.innerHTML += 'Carteri, P. “IQ ja psühhomeetrilised testid” (London, 2007). Eesti keelde tõlkinud Kreet Volmer.';
    document.getElementById("fdiv").appendChild(allt2);

    document.querySelector('form.t2').addEventListener('submit', (e) => {
        if (!saadetud) {
            var end = new Date().getTime();
            getT2Testdata(e, end-start);
            saadetud = true;
            clearTimeout(cltimeout);
            //remT2(saadetud);
            vaheNupp2(saadetud);
        }
        e.preventDefault();
    });

    //if (!saadetud) cltimeout = setTimeout(remT2, audioduration2, saadetud);
    if (!saadetud) cltimeout = setTimeout(vaheNupp2, audioduration2, saadetud);
}

function vaheNupp2(saadetud) {
    if (!saadetud) {
        document.getElementById('conf').click();
    }
    saadetud = false;
    document.getElementById("f1").remove();
    document.getElementById("lugu").remove();
    document.getElementById("lugu2").remove();

    var julge = document.createElement("p");
    julge.id = "julge";
    julge.textContent = "Oled testi sooritanud, aeg on asuda kuulamisülesande juurde.";
    document.getElementById("yl").appendChild(julge);

    var e1 = document.createElement("button");
    e1.type = "button";
    e1.className = "btn btn-primary btn-sm";
    e1.id = "e1";
    e1.textContent = "Alustan kuulamisülesannet";
    document.getElementById("yl").appendChild(e1);

    //Button listener T1 alustamise nupule
    const switcher2 = document.querySelector('#e1');
    switcher2.addEventListener('click', function() {
        document.getElementById("julge").remove();
        document.getElementById("e1").remove();
        remT2(saadetud);
    });
}

//Abifunktsioon järjestikkuse testi lõpetamiseks T2 ja kuulamisülesande käivitamiseks
function remT2(saadetud) {
    /*if (!saadetud) {
        document.getElementById('conf').click();
    }
    saadetud = false;

    document.getElementById("f1").remove(); 
    document.getElementById("lugu").remove();
    document.getElementById("lugu2").remove();*/

    audio2 = new Audio(audiofile2);
    audio2.addEventListener("canplaythrough", event => {
        audio2.play();
        audio2.addEventListener("play", function(e){
            var lugu = document.createElement("p");
            lugu.id = "lugu";
            lugu.textContent = 'Kuulamisülesanne: ';
            document.getElementById("yl").appendChild(lugu);
            var lugu1 = document.createElement("p");
            lugu1.id = "lugu1";
            lugu1.innerHTML = 'Kuulete artiklit "Rootsi lapsed õpivad koolitunnis leiutama". Loeb Tõnis Hendrik Hlebnikov.<br>';
            document.getElementById("yl").appendChild(lugu1);
            var allk2 = document.createElement("p");
            allk2.id = "allk2";
            allk2.innerHTML = 'Allikas: https://eis.ekk.edu.ee/eis/lahendamine/3302/edit';
            document.getElementById("yl").appendChild(allk2);
            var lugu2 = document.createElement("p");
            lugu2.id = "lugu2";
            lugu2.textContent = "Kuula lugu ära ja vasta seejärel küsimustele. Küsimustele vastamine on ajaliselt piiratud."
            document.getElementById("yl").appendChild(lugu2);
        });
    });
    audio2.addEventListener("ended", function(e){
        document.getElementById("lugu").remove();
        document.getElementById("lugu1").remove();
        document.getElementById("lugu2").remove();    

        var f = document.createElement("form");
        f.id = "f1";
        f.className = "k2";
        f.name = "k2";
        document.getElementById("yl").appendChild(f);
    
        var c = 0;
        while (c < 5) {
            nextQ(c, lugkys2, lugvastused2, "lugv");
            c++;
        }
        
        var startK = new Date().getTime();
        var bt = document.createElement("input");
        bt.type = "submit";
        bt.className = "btn btn-primary btn-sm";
        bt.id ="conf";
        bt.value = "Lõpeta";
        document.getElementById("fdiv").appendChild(bt);

        document.querySelector('form.k2').addEventListener('submit', (e) => {
            if (!saadetud) {
                var endK = new Date().getTime();
                getK2data(e, endK-startK);
                saadetud = true;
                clearTimeout(cltimeout);
                remQ2(saadetud, startK);
            }
            e.preventDefault();
        });
    
        if (!saadetud) cltimeout = setTimeout(remQ2, timeforkuulamine, saadetud, startK);
    });
}

function remQ2(saadetud, startK) {
    if (!saadetud) {
        document.getElementById('conf').click();

        document.querySelector('form.k2').addEventListener('submit', (e) => {
            //korjatakse paralleelselt vastatud T1 vastused
            var endK = new Date().getTime();
             getK2data(e, endK-startK);
             saadetud = true;
             e.preventDefault();
        });
    }
    document.getElementById("second").remove();
    if (esimene === 0) {
        pageEnd();
    } else {
        pageT1();
    }
    
}

function pageEnd(){
    var br = document.createElement("br");

    var pend = document.createElement("div");
    pend.id = "pend";
    document.getElementById("main").appendChild(pend);

    var terv1 = document.createElement("h1");
    terv1.id = "terv1";
    terv1.textContent = "Lõpp on lähedal!";
    document.getElementById("pend").appendChild(terv1);

    var frm = document.createElement("div");
    frm.id = "frm";
    document.getElementById("pend").appendChild(frm);

    var p1 = document.createElement("p");
    p1.textContent = "Palun vasta veel viimastele küsimustele selle kohta, kuidas testide täitmine sujus. Kuna teste ei viia läbi ühetaolistest kontrollitud tingimustes, on oluline, et me teaksime, kui juhtus midagi, mis võib tulemust mõjutada.";
    document.getElementById("frm").appendChild(p1);

    var f = document.createElement("form");
    f.id = "f";
    document.getElementById("frm").appendChild(f);

    var dv10 = document.createElement('div');
    dv10.className = "form-check";
    dv10.id = "sooritus";
    document.getElementById("f").appendChild(dv10);

    var lb14 = document.createElement('label');
    lb14.className = "lbkys";
    lb14.textContent = "1. Kas sa said testi sooritada nii, et keegi ei häirinud?";
    document.getElementById("sooritus").appendChild(lb14);

    var dvjah = document.createElement('div');
    dvjah.id = "dvjah";
    document.getElementById("sooritus").appendChild(dvjah);

    var in4 = document.createElement('input');
    in4.type = "radio";
    in4.id = "jah";
    in4.name = "tekst";
    in4.value = "jah";
    in4.required = true;
    document.getElementById("dvjah").appendChild(in4);

    var lb15 = document.createElement('label');
    lb15.for = "jah";
    lb15.textContent = "jah";
    lb15.className = "lbradio";
    document.getElementById("dvjah").appendChild(lb15);

    var dvei = document.createElement('div');
    dvei.id = "dvei";
    document.getElementById("sooritus").appendChild(dvei);
    
    var in5 = document.createElement('input');
    in5.type = "radio";
    in5.id = "ei";
    in5.name = "tekst";
    in5.value = "ei";
    document.getElementById("dvei").appendChild(in5);

    var lb16 = document.createElement('label');
    lb16.for = "ei";
    lb16.textContent = "ei";
    lb16.className = "lbradio";
    document.getElementById("dvei").appendChild(lb16);

    var sel = false;

     //Täpsustuse kirjutamise võimalus lehele end.html
    //if (document.getElementById("end")) {
    $('input:radio').on('click', function(e) {
        var name = e.currentTarget.name; 
        var txt = e.currentTarget.value; 
        var br = document.createElement('br')
        
        if(txt=="ei" && sel == false) {
            var dv = document.createElement('div')
            dv.id = "pohjus"
            document.getElementById("sooritus").appendChild(dv);
            document.getElementById("sooritus").appendChild(br);
            var lbl = document.createElement("label");
            lbl.id = "soorituslb"
            lbl.textContent = "Kuna Sa ütlesid, et Sind häiriti, siis palun täpsusta, milliste osade juures: ";
            document.getElementById("pohjus").appendChild(lbl);
            document.getElementById("pohjus").appendChild(br);
            var muu = document.createElement("input");
            muu.type = "text";
            muu.id = "muupohjus";
            muu.className = "sega";
            muu.name = "segamuu";
            document.getElementById("pohjus").appendChild(muu);
            sel = true;
        } else if (txt=="jah" && sel) {
            document.getElementById("pohjus").remove();
            sel = false;
        }
    });

    var segadus1 = document.createElement("div");
    segadus1.id = "segadus1";
    segadus1.className = "form-check";
    document.getElementById("f").appendChild(segadus1);

    var lb17 = document.createElement("label");
    lb17.className = "lbkys";
    lb17.textContent = "2. Kas Sind ajas ülesannete juures miski segadusse? Mis?";
    document.getElementById("segadus1").appendChild(lb17);

    var in6 = document.createElement("input");
    in6.type = "text";
    in6.className = "sega";
    in6.name = "sega1";
    document.getElementById("segadus1").appendChild(in6);

    var segadus2 = document.createElement("div");
    segadus2.id = "segadus2";
    segadus2.className = "form-check";
    document.getElementById("f").appendChild(segadus2);

    var lb18 = document.createElement("label");
    lb18.className = "lbkys";
    lb18.textContent = "3. Kas on midagi, mis võis mõjutada testi lahendamise kiirust või saadud tulemusi?";
    document.getElementById("segadus2").appendChild(lb18);

    var in7 = document.createElement("input");
    in7.type = "text";
    in7.className = "sega";
    in7.name = "sega2";
    document.getElementById("segadus2").appendChild(in7);

    var multi = document.createElement('div');
    multi.className = "form-check";
    multi.id = "multi";
    document.getElementById("f").appendChild(multi);

    var lb19 = document.createElement('label');
    lb19.className = "lbkys";
    lb19.textContent = "4. Kas Sa oled puutunud taoliste rööprähklemist nõudvate ülesannetega varem kokku?";
    document.getElementById("multi").appendChild(lb19);

    var dv41 = document.createElement('div');
    dv41.id = "dv41";
    document.getElementById("multi").appendChild(dv41);

    var in8 = document.createElement('input');
    in8.type = "radio";
    in8.id = "41";
    in8.name = "dual";
    in8.value = "igapäevaselt";
    in8.required = true;
    document.getElementById("dv41").appendChild(in8);

    var lb20 = document.createElement('label');
    lb20.for = "41";
    lb20.className = "lbradio";
    lb20.textContent = "igapäevaselt";
    document.getElementById("dv41").appendChild(lb20);

    var dv42 = document.createElement('div');
    dv42.id = "dv42";
    document.getElementById("multi").appendChild(dv42);

    var in9 = document.createElement('input');
    in9.type = "radio";
    in9.id = "42";
    in9.name = "dual";
    in9.value = "mõned korrad nädalas";
    document.getElementById("dv42").appendChild(in9);

    var lb21 = document.createElement('label');
    lb21.for = "42";
    lb21.className = "lbradio";
    lb21.textContent = "mõned korrad nädalas";
    document.getElementById("dv42").appendChild(lb21);

    var dv43 = document.createElement('div');
    dv43.id = "dv43";
    document.getElementById("multi").appendChild(dv43);

    var in10 = document.createElement('input');
    in10.type = "radio";
    in10.id = "43";
    in10.name = "dual";
    in10.value = "kord nädalas";
    document.getElementById("dv43").appendChild(in10);

    var lb22 = document.createElement('label');
    lb22.for = "43";
    lb22.className = "lbradio";
    lb22.textContent = "kord nädalas";
    document.getElementById("dv43").appendChild(lb22);

    var dv44 = document.createElement('div');
    dv44.id = "dv44";
    document.getElementById("multi").appendChild(dv44);

    var in11 = document.createElement('input');
    in11.type = "radio";
    in11.id = "44";
    in11.name = "dual";
    in11.value = "kord kuus";
    document.getElementById("dv44").appendChild(in11);

    var lb23 = document.createElement('label');
    lb23.for = "44";
    lb23.className = "lbradio";
    lb23.textContent = "kord kuus";
    document.getElementById("dv44").appendChild(lb23);

    var dv45 = document.createElement('div');
    dv45.id = "dv45";
    document.getElementById("multi").appendChild(dv45);

    var in12 = document.createElement('input');
    in12.type = "radio";
    in12.id = "45";
    in12.name = "dual";
    in12.value = "tavaliselt üldse mitte";
    document.getElementById("dv45").appendChild(in12);

    var lb24 = document.createElement('label');
    lb24.for = "45";
    lb24.className = "lbradio";
    lb24.textContent = "tavaliselt üldse mitte";
    document.getElementById("dv45").appendChild(lb24);
    
    var p2 = document.createElement("p");
    p2.textContent = "Olemegi jõudnud lõppu. Esita vastused ja saa teada oma antud korrektsete vastuste osakaal. Täname, et Sa leidsid eksperimendis osalemise jaoks aega!";
    document.getElementById("f").appendChild(p2);

    var end = document.createElement("input");
    end.type = "submit";
    end.className = "btn btn-primary btn-sm";
    end.id = "end";
    end.value = "Esita!";
    document.getElementById("f").appendChild(end);

    var T1 = ["realistlik",
            "jõhvikas",
            "autentne",
            "kadakas",
            "sepp",
            "riku",
            "truu",
            "puudus",
            "hobune",
            "piisad"];
    var N1 = [T10, T11, T12, T13, T14, T15, T16, T17, T18, T19];

    var K1 = ["See lugu räägib sellest, kuidas võis sündida üks suur Eesti jõgi.",
            "Sellepärast, et igaüks elust rõõmu tunneks.",
            "Loomad ei saanud omavahel läbi.",
            "Sellepärast, et hunt töötas oma koonuga palju.",
            "Sellepärast, et ta käitus Vanaisaga nipsakalt."];
    var N2 = [K10, K11, K12, K13, K14];

    var T2 = [
        "D",
        "A",
        "B",
        "A",
        "D",
        "A",
        "E",
        "B",
        "D",
        "D"
    ];
    var N3 = [T20, T21, T22, T23, T24, T25, T26, T27, T28, T29];

    var K2 = [
        "Sellest, kuidas lapsed leiutavad.",
        "Lapsed leiutavad seepärast, et lahendada igapäevaelu probleeme.",
        "Leiutamisvõistlusel osalemine on populaarne.",
        "Joogipudel on edukas, sest seda on mugav täita.",
        "Laste loovus muutub."
    ]

    var N4 = [K20, K21, K22, K23, K24];
    var tul1 = 0
    for (var i = 0; i < N1.length; i++) {
        if (N1[i] == T1[i]) tul1++;
    }

    var tul2 = 0
    for (i = 0; i < N2.length; i++) {
        if (N2[i] == K1[i]) tul2++;
    }

    var tul3 = 0
    for (i = 0; i < N3.length; i++) {
        if (N3[i] == T2[i]) tul3++;
    }

    var tul4 = 0
    for (i = 0; i < N4.length; i++) {
        if (N4[i] == K2[i]) tul4++;
    }

    document.querySelector('form').addEventListener('submit', (e) => {
        const formData = new FormData(e.target);
        E1 = formData.get("tekst");
        //console.log(E1, formData.get("tekst"));
        E11 = formData.get("segamuu");
        //console.log(E1, formData.get("tekst"));
        E2 = formData.get("sega1");
        //console.log(E2, formData.get("sega1"));
        E3 = formData.get("sega2");
        //console.log(E3, formData.get("sega2"));
        E4 = formData.get("dual");
        //console.log(E4, formData.get("dual"));

        e.preventDefault();

        var testVal = {
        "entry.1838707956": ID,
        "entry.929674135":	E1,
        "entry.1009494262": E11,
        "entry.1785665696":	E2,
        "entry.1664735305":	E3,
        "entry.158839874":	E4,
        "entry.1440885999": tul1,
        "entry.1206742145": tul2,
        "entry.1381603350": tul3,
        "entry.1572036422": tul4,
        }
        $.ajax({
            url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLScTA_QKxOC7aC6ZyT0gMz2f89DWq-DF2kH5YHVWBe6sa2PeQw/formResponse",
            data: testVal,
            type: "POST",
            async: false,
            dataType: "xml",
            statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
            }
        });
    document.getElementById("frm").remove();

    terv1.textContent = "Lõpp!";
 
    var tul = document.createElement("div");
    tul.id = "tulemused";
    document.getElementById("pend").appendChild(tul);

    //var br = document.createElement('br');
    document.getElementById("tulemused").appendChild(br);
    var tul = document.createElement('p');
    tul.textContent = "Oleme väga tänulikud!";
    tul.className ="tul";
    document.getElementById("tulemused").appendChild(tul);
    document.getElementById("tulemused").appendChild(br);
    
    tul = document.createElement('p');
    tul.textContent = "Sinu tulemused: ";
    tul.className ="tul";
    document.getElementById("tulemused").appendChild(tul);
    
    tul = document.createElement('p');
    tul.textContent = "Kuulamisülesanne 1: " + String(tul2) + "/5";
    tul.className = "tul";
    document.getElementById("tulemused").appendChild(tul);
    //var br = document.createElement('br')

    tul = document.createElement('p');
    tul.textContent = "Test 1: " + String(tul1) + "/10";
    tul.className = "tul";
    document.getElementById("tulemused").appendChild(tul);
    
    tul = document.createElement('p');
    tul.textContent = "Kuulamisülesanne 2: " + String(tul4) + "/5";
    tul.className = "tul";
    document.getElementById("tulemused").appendChild(tul);

    tul = document.createElement('p');
    tul.textContent = "Test 2: " + String(tul3) + "/10";
    tul.className = "tul";
    document.getElementById("tulemused").appendChild(tul);
    });
}

function getK1data(e, time) {
    const formData = new FormData(e.target);
    K10 = formData.get("tt0");
    //console.log(K10, formData.get("tt0"));
    K11 = formData.get("tt1");
    //console.log(K11, formData.get("tt1"));
    K12 = formData.get("tt2");
    //console.log(K12, formData.get("tt2"));
    K13 = formData.get("tt3");
    //console.log(K13, formData.get("tt3"));
    K14 = formData.get("tt4");
    //console.log(K14, formData.get("tt4"));
        
    var testVal = {
        "entry.1146599562": ID,
        "entry.97909952": testgroup,
        "entry.954162772": esimene,
        "entry.178421692": K10,
        "entry.1877055364": K11,
        "entry.2118608732": K12,
        "entry.1163987264": K13,
        "entry.1117502128": K14,
        "entry.1376272657": time,
    };
        
    $.ajax({
            url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSejMqEbI3Epf1cscb-Tij1Ggd9Dsp-j2H-lo7FAnmslyVRojQ/formResponse",
            data: testVal,
            type: "POST",
            async: false,
            dataType: "xml",
            statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
            }
    });     
}

function getT1Testdata(e, time) {

    const formData = new FormData(e.target);
    T10 = formData.get("tt0");
    //console.log(T10, formData.get("tt0"));
    T11 = formData.get("tt1");
    //console.log(T11, formData.get("tt1"));
    T12 = formData.get("tt2");
    //console.log(T12, formData.get("tt2"));
    T13 = formData.get("tt3");
    //console.log(T13, formData.get("tt3"));
    T14 = formData.get("tt4");
    //console.log(T14, formData.get("tt4"));
    T15 = formData.get("tt5");
    //console.log(T15, formData.get("tt5"));
    T16 = formData.get("tt6");
    //console.log(T16, formData.get("tt6"));
    T17 = formData.get("tt7");
    //console.log(T17, formData.get("tt7"));
    T18 = formData.get("tt8");
    //console.log(T18, formData.get("tt8"));
    T19 = formData.get("tt9");
    //console.log(T19, formData.get("tt9"));
        
    var testVal = {
            "entry.1197279231" : ID,
            "entry.1920649068" : testgroup,
            "entry.133142997" : esimene,
            "entry.2073370074" : T10, 
            "entry.1367500775" : T11,
            "entry.569777228" : T12,
            "entry.1901945329" : T13,
            "entry.693655740" : T14,
            "entry.1610082585" : T15,
            "entry.2033710157" : T16,
            "entry.1929431787" : T17,
            "entry.2048846158" : T18,
            "entry.1210306947" : T19,
            "entry.317605660": time,
                };
        
    $.ajax({
            url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSe9dIOrMTrQf-ecdQLwlzRDDk_JHXSEJO-szD_YD6R6XRTqrA/formResponse",
            data: testVal,
            type: "POST",
            dataType: "xml",
            async: false,
            statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
            }
    });     
}

function getT2Testdata(e, time) {

    const formData = new FormData(e.target);
    T20 = formData.get("tt0");
    //console.log(T20, formData.get("tt0"));
    T21 = formData.get("tt1");
    //console.log(T21, formData.get("tt1"));
    T22 = formData.get("tt2");
    //console.log(T22, formData.get("tt2"));
    T23 = formData.get("tt3");
    //console.log(T23, formData.get("tt3"));
    T24 = formData.get("tt4");
    //console.log(T24, formData.get("tt4"));
    T25 = formData.get("tt5");
    //console.log(T25, formData.get("tt5"));
    T26 = formData.get("tt6");
    //console.log(T26, formData.get("tt6"));
    T27 = formData.get("tt7");
    //console.log(T27, formData.get("tt7"));
    T28 = formData.get("tt8");
    //console.log(T28, formData.get("tt8"));
    T29 = formData.get("tt9");
    //console.log(T29, formData.get("tt9"));
        
    var testVal = {
        "entry.1171571539": ID,
        "entry.1892876233": testgroup,
        "entry.1429285732": esimene,
        "entry.1827211388": T20,
        "entry.2102750581": T21,
        "entry.1570640604": T22,
        "entry.779407761": T23,
        "entry.1885775743": T24,
        "entry.742132310": T25,
        "entry.1222081398": T26,
        "entry.522711002": T27,
        "entry.1230909676": T28,
        "entry.1128789443": T29,
        "entry.1670866961": time,
    };
        
    $.ajax({
            url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSfEhW1JsQBsd-nXHQhtqd-MM6CWTyer2q5Y6BARryUsZBfzPQ/formResponse",
            data: testVal,
            type: "POST",
            dataType: "xml",
            async: false,
            statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
            }
    });     
}

function getK2data(e, time) {
    const formData = new FormData(e.target);
    K20 = formData.get("tt0");
    //console.log(K20, formData.get("tt0"));
    K21 = formData.get("tt1");
    //console.log(K21, formData.get("tt1"));
    K22 = formData.get("tt2");
    //console.log(K22, formData.get("tt2"));
    K23 = formData.get("tt3");
    //console.log(K23, formData.get("tt3"));
    K24 = formData.get("tt4");
    //console.log(K24, formData.get("tt4"));
        
    var testVal = {
        "entry.1451167264": ID,
        "entry.88644397": testgroup,
        "entry.399674961": esimene,
        "entry.1752387505": K20,
        "entry.1849498046": K21,
        "entry.2145166307": K22,
        "entry.74586952": K23,
        "entry.1758221152": K24,
        "entry.1595881021": time
        };
        
    $.ajax({
            url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSfBEvYP-Pf8em0ylvFIaDdgRFcpBKYlXNN6Ctlg8uUn_u5C9w/formResponse",
            data: testVal,
            type: "POST",
            async: false,
            dataType: "xml",
            statusCode: {
                    0: function() {
                        //Success message
                    },
                    200: function() {
                        //Success Message
                    }
            }
    });     
}