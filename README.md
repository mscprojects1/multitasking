# Experiment to probe the effect of multitasking on learning

This is a project for the Introduction to Computational Neuroscience course in University of Tartu

Project instructor: Jaan Aru

Project team members: Kairit Peekman, Kaire Koljal, Rytis Valiukas, Tõnis Hendrik Hlebnikov

The project blog will be at:
https://gitlab.com/mscprojects1/multitasking/-/wikis/Experiment-to-Probe-the-Effect-of-Multitasking-on-Learning


Test environment:
https://mscprojects1.gitlab.io/multitasking/

